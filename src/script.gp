# Nouveau script pour le tracé avec trois colonnes
set datafile separator ","
set title "Temps d'exécution des algorithmes"
set xlabel "Nombre de points"
set ylabel "Temps d'exécution (microsecondes)"

# Tracé des courbes
plot "naif_welzl_micro.csv" using 1:2 with linespoints title "Algorithme Naïf", \
     "" using 1:3 with linespoints title "Algorithme de Welzl"

# Pause pour maintenir la fenêtre ouverte
pause -1 "Appuyez sur n'importe quelle touche pour quitter..."
