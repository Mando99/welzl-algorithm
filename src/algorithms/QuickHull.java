package algorithms;

import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;

public class QuickHull {

    private static Point[] getExtremums(ArrayList<Point> points) {
        if (points.size() < 4) return null; // Il faut au moins 4 points pour définir un quadrilatère

        Point[] extremums = new Point[4];

        // Trouver le point le plus à gauche
        Point leftmost = points.get(0);
        for (Point point : points) {
            if (point.x < leftmost.x || (point.x == leftmost.x && point.y < leftmost.y)) {
                leftmost = point;
            }
        }
        extremums[0] = leftmost;

        // Trouver le point le plus à droite
        Point rightmost = points.get(0);
        for (Point point : points) {
            if (point.x > rightmost.x || (point.x == rightmost.x && point.y > rightmost.y)) {
                rightmost = point;
            }
        }
        extremums[1] = rightmost;

        Point top = points.get(0);
        for (Point point : points) {
            if (point.y < top.y || (point.y == top.y && point.x > top.x)) {
                top = point;
            }
        }
        extremums[2] = top;

        Point bottom = points.get(0);
        for (Point point : points) {
            if (point.y > bottom.y || (point.y == bottom.y && point.x < bottom.x)) {
                bottom = point;
            }
        }
        extremums[3] = bottom;

        return extremums;
    }

    public static ArrayList<Point> QH(ArrayList<Point> points) {
        if (points.size() < 4) return null; // Un quadrilatère Akl-Toussaint a besoin d'au moins 4 points

        // Initialiser les points extrêmes du quadrilatère Akl-Toussaint
        Point[] extremums = getExtremums(points);
        Point A = extremums[0];
        Point B = extremums[1];
        Point C = extremums[2];
        Point D = extremums[3];

        // Initialiser la liste des points du polygone convexe
        ArrayList<Point> convexHull = new ArrayList<>();
        convexHull.add(A);
        convexHull.add(B);
        convexHull.add(C);
        convexHull.add(D);

        // Appliquer l'algorithme QuickHull récursivement
        quickHull(points, A, B, convexHull);
        quickHull(points, B, C, convexHull);
        quickHull(points, C, D, convexHull);
        quickHull(points, D, A, convexHull);

        return convexHull;
    }

    private static void quickHull(ArrayList<Point> points, Point A, Point B, ArrayList<Point> convexHull) {
        ArrayList<Point> pointsLeft = new ArrayList<>();
        for (Point point : points) {
            if (isLeftOfLine(A, B, point)) {
                pointsLeft.add(point);
            }
        }

        if (!pointsLeft.isEmpty()) {
            Point C = getFarthestPoint(A, B, pointsLeft);
            convexHull.add(convexHull.indexOf(B), C);
            quickHull(pointsLeft, A, C, convexHull);
            quickHull(pointsLeft, C, B, convexHull);
        }
    }

    private static boolean isLeftOfLine(Point A, Point B, Point point) {
        return (B.x - A.x) * (point.y - A.y) - (B.y - A.y) * (point.x - A.x) > 0;
    }

    private static Point getFarthestPoint(Point A, Point B, ArrayList<Point> points) {
        double maxDistance = -1;
        Point farthest = null;

        for (Point point : points) {
            double distance = Line2D.ptLineDist(A.x, A.y, B.x, B.y, point.x, point.y);
            if (distance > maxDistance) {
                maxDistance = distance;
                farthest = point;
            }
        }

        return farthest;
    }

}
