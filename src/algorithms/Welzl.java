package algorithms;

import supportGUI.Circle;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

public class Welzl {

    public static Circle welzl(ArrayList<Point> points) {
        ArrayList<Point> shuffledPoints = new ArrayList<>(points);
        Collections.shuffle(shuffledPoints); // on mélange aléatoirement la liste
        return welzl(shuffledPoints, new ArrayList<>());
    }

    private static Circle welzl(ArrayList<Point> P, ArrayList<Point> R) {
        Circle D = new Circle(new Point(0, 0), 0);
        if (P.isEmpty() || R.size() == 3) {
            return b_md(new ArrayList<>(), R); // trois points sont sur le cercle, cas trivial
        } else {
            Point p = P.remove((int) (Math.random() * P.size())); // on récupère et on supprime le point choisi aléatoirement (P - {p})
            D = welzl(P, R); // appel récursif sur P et R
            if (D != null && !isInside(p, D)) { // si D est bien défini et ne contient pas P
                R.add(p); // Ajouter p à R
                D = welzl(P, R);
                R.remove(p);
            }
        }
        return D;
    }

    public static Circle b_md(ArrayList<Point> P, ArrayList<Point> R) {
        if (P.isEmpty() && R.size() == 0)
            return new Circle(new Point(0, 0), 0);

        Circle D = new Circle(new Point(0, 0), 0);
        if (R.size() == 1) {
            D = new Circle(R.get(0), 0); // Un point : cercle de centre ce point là
        }

        if (R.size() == 2) { // Deux points, cercle de diamètre ces deux points là
            double cx = (R.get(0).x + R.get(1).x) / 2.;
            double cy = (R.get(0).y + R.get(1).y) / 2.;
            double d = R.get(0).distance(R.get(1)) / 2;
            Point p = new Point((int) cx, (int) cy);
            D = new Circle(p, (int) Math.ceil(d));
        }
        else {
            if (R.size() == 3) // 3 points, il suffit de calculer le cercle circonscrit
                D = circle3point(R.get(0), R.get(1), R.get(2));
        }
        return D;
    }

    private static int norme(Point a) {
        return (a.x * a.x) + (a.y * a.y);
    }

    private static Circle circle3point(Point a, Point b, Point c) { // Calcul du cercle circonscrit des trois points
        double d = (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y)) * 2;
        if (d == 0)
            return new Circle(new Point(0, 0), 0);

        double x = ((norme(a) * (b.y - c.y)) + (norme(b) * (c.y - a.y)) + (norme(c) * (a.y - b.y))) / d;
        double y = ((norme(a) * (c.x - b.x)) + (norme(b) * (a.x - c.x)) + (norme(c) * (b.x - a.x))) / d;
        Point p = new Point((int) x, (int) y);

        return new Circle(p, (int) Math.ceil(p.distance(a)));
    }

    private static boolean isInside(Point p, Circle circle) {
        return circle != null && p.distance(circle.getCenter()) <= circle.getRadius();
    }

}
