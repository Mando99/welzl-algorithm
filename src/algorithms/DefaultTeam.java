package algorithms;

import java.awt.Point;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import supportGUI.Circle;
import supportGUI.Line;

import static algorithms.EnclosingCircle.algoNaif;
import static algorithms.Welzl.welzl;

public class DefaultTeam {

  // calculDiametre: ArrayList<Point> --> Line
  //   renvoie une paire de points de la liste, de distance maximum.
  public Line calculDiametre(ArrayList<Point> points) {
    if (points.size()<2) return null;
    Point p=points.get(0);
    Point q=points.get(1);
    for (Point s: points) for (Point t: points) if (s.distance(t)>p.distance(q)) {p=s;q=t;}
    return new Line(p,q);
  }

  // calculCercleMin: ArrayList<Point> --> Circle
  //   renvoie un cercle couvrant tout point de la liste, de rayon minimum.
  public Circle calculCercleMin(ArrayList<Point> points) {
    //ArrayList<Point> convexHull = QH(points); //filtre QuickHull
    //assert convexHull != null;
    return welzl(points);
  }

  // génère fichier res avec les temps d'exec naif et welzl sur n fichiers (256 * n points)
  // il peut y avoir des doublons
  public static void run(int n) {

    try (PrintWriter writer = new PrintWriter("src/res.csv")) {

      ArrayList<Point> points = new ArrayList<>();

      // on parcourt les n fichiers
      for(int i = 2; i <= n; i++) { //on commence à 2 car 1er fichier mal formaté
        String filename = "src/samples/test-" + i + ".points";

        try {
          Path filePath = Paths.get(filename);
          BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(filePath.toFile())));

          try {
            String line;
            // on ajoute chaque point à la liste
            while((line = input.readLine()) != null) {
              String[] coordinates = line.split("\\s+");
              points.add(new Point(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1])));
            }

          } catch (IOException var16) {
            System.err.println("Exception: interrupted I/O.");
          } finally {
            try {
              input.close();
            } catch (IOException var14) {
              System.err.println("I/O exception: unable to close " + filename);
            }

          }
        } catch (FileNotFoundException var18) {
          System.err.println("Input file not found.");
        }

        // on supprime les doublons
        Set<Point> uniquePoints = new LinkedHashSet<>(points);
        ArrayList<Point> uniquePointsList = new ArrayList<>(uniquePoints);

        // calculer le temps d'execution algo naif en microsecondes
        long startTime1 = System.nanoTime();
        Circle res1 = algoNaif(uniquePointsList);
        long endTime1 = System.nanoTime();
        long executionTime1 = (endTime1 - startTime1) / 1000;

        // calculer le temps d'execution algo welzl en microsecondes
        long startTime2 = System.nanoTime();
        Circle res2 = welzl(uniquePointsList);
        long endTime2 = System.nanoTime();
        long executionTime2 = (endTime2 - startTime2) / 1000;

        // Écrit les temps d'execution dans le fichier res
        String res = uniquePointsList.size() + "," + executionTime1 + "," + executionTime2;
        writer.println(res);
        System.out.println(res);
      }

    } catch (FileNotFoundException var16) {
      System.err.println("Exception: interrupted I/O.");
    }

  }

  public static void main(String[] args) {
    run(Integer.parseInt(args[0]));
  }

}