# README

## Projet Welzl

Ce README fournit des instructions sur la manière de lancer l'application Java, que ce soit depuis IntelliJ IDEA en configurant l'application de lancement ou en ligne de commande.

### Prérequis

Avant de commencer, assurez-vous d'avoir les éléments suivants installés sur votre machine :

- Java Development Kit (JDK) version 8 ou supérieure.
- IntelliJ IDEA (facultatif, mais recommandé pour une meilleure expérience de développement).

### Lancement depuis IntelliJ IDEA

1. Configurez l'application de lancement :
   - Créer une application de lancement.
   - Choisissez `DefaultTeam`.
   - Dans la fenêtre de configuration, ajoutez le nombre de fichiers comme paramètre de programme.

2. Exécutez l'application en cliquant sur le bouton "Run" dans IntelliJ IDEA.

### Lancement depuis la ligne de commande

1. Ouvrez un terminal.
2. Naviguez vers le répertoire contenant les fichiers source de votre application.
3. Compilez la classe `DefaultTeam` avec la commande suivante :
   - `javac DefaultTeam.java`
   - `java DefaultTeam <nombre_de_fichiers>`

Assurez-vous de remplacer <nombre_de_fichiers> par un entier correspondant au nombre de fichiers que vous souhaitez lire.

### Lancement de la vue graphique

1. Configurez la configuration de l'application de lancement :
   - Créer une application de lancement.
   - Choisissez `supportGUI.DiameRace`

2. Exécutez l'application en cliquant sur le bouton "Run" dans IntelliJ IDEA.